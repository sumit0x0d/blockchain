#include "blockchain.h"
#include "block/block.h"

#include <stdlib.h>

Blockchain* blockchain_Create(size_t dataSize)
{
    Blockchain* blockchain = (Blockchain*)malloc(sizeof (Blockchain));
    if(!blockchain) {
        return NULL;
    }
    blockchain->genesis = NULL;
    blockchain->dataSize = dataSize;
    blockchain->size = 0;
    return blockchain;
}

void Blockchain_Insert(struct Blockchain* blockchain, void* data)
{
    // Block* block = BlockCreate(data, data_size, B->tail->hash);
    blockchain->size = blockchain->size + 1;
}
