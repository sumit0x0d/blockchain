#include "sha-256/sha-256.h"
#include "block.h"

#include <stdlib.h>
#include <string.h>
#include <time.h>

static const char* getMessage(void* data, size_t dataSize, uint8_t previousHash[32])
{
    return "test";
}

Block* Block_Create(size_t dataSize, uint8_t* blockchainHash)
{
    Block* block = (Block*)malloc(sizeof (Block));
    if(!block) {
        return NULL;
    }
    block->data = malloc(dataSize);
    block->next = NULL;
    return block;
}
