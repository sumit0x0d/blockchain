#ifndef SHA_256_H
#define SHA_256_H

#include <stdint.h>

uint8_t* Sha256(const char* message);

#endif
