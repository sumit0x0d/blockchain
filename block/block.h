#ifndef BLOCK_H
#define BLOCK_H

#include <stdint.h>
#include <stdlib.h>
#include <time.h>

typedef struct Block {
    void* data;
    uint8_t* previousHash;
    time_t timestamp;
    struct Block* next;
} Block;

Block* Block_Create(void* data, size_t dataSize, uint8_t* blockchainHash);

#endif
