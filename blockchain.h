#ifndef BLOCKCHAIN_H
#define BLOCKCHAIN_H

#include <stddef.h>

typedef struct Blockchain {
    struct Block* genesis;
    size_t dataSize;
    size_t size;
} Blockchain;

Blockchain* Blockchain_Create(size_t dataSize);

void Blockchain_Insert(Blockchain* blockchain, void* data);

#endif
