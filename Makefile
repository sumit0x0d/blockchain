CC = clang

TARGET = main

CFLAGS = -std=c99 -O0 -g -Wall -Wpedantic -Wextra -Werror

all:
	$(CC) $(CFLAGS) \
	block/block.c \
	block/sha-256/sha-256.c \
	blockchain.c \
	main.c -o $(TARGET)

clean:
	rm $(TARGET)
