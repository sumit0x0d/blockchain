#include "blockchain.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(void)
{	
	srand((int)time(NULL));
	Blockchain* blockchain = Blockchain_Create(sizeof (int));
	if (!blockchain) {
		printf("ERROR: Blockchain_Create()\n");
		return 1;
	}
	printf("Blockchain Size: %zu\n", blockchain->size);
	return 0;
}
